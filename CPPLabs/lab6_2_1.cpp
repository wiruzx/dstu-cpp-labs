//
//  lab6_2_1.cpp
//  CPPLabs
//
//  Created by Виктор Шаманов on 1/23/14.
//  Copyright (c) 2014 Victor Shamanov. All rights reserved.
//

#include "lab6_2_1.h"
#include <stdlib.h>
#include <iostream>

static float const PI = 3.14159265;

class Figure
{
public:
    virtual double area() = 0;
    virtual void show() = 0;
};

class Circle: public Figure
{
    double radius;
public:
    Circle(double r)
    {
        if (r <= 0) {
            std::cout << "Radius must be more than zero" << std::endl;
            exit(1);
        }
        
        radius = r;
    }
    
    double area()
    {
        return PI * radius * radius;
    }
    
    double& get_radius()
    {
        return radius;
    }
    
    void show()
    {
        std::cout << "Circle, radius = " << radius << std::endl;
        std::cout << "Area = " << area() << std::endl;
    }
};

class Rectangle: public Figure
{
    double width;
    double height;
public:
    Rectangle(double w = 0, double h = 0)
    {
        width = w;
        height = h ? h : w;
    }
    
    void show()
    {
        if (width == height) {
            std::cout << "Square, side = " << width << std::endl;
        } else {
            std::cout << "Rectangle, width = " << width << " height = " << height << std::endl;
        }
        std::cout << "Area = " << area() << std::endl;
    }
    
    double area()
    {
        return width * height;
    }
    
    double& get_width()
    {
        return width;
    }
    
    double& get_height()
    {
        return height;
    }
};

void lab6_2_1()
{
    Circle circles[5] = {
        Circle(10),
        Circle(20),
        Circle(30),
        Circle(40),
        Circle(50),
    };
    
    for (int i = 0; i < 5; i++) {
        std::cout << "Circle at index " << i << ":" << std::endl;
        std::cout << "Size of object = " << sizeof(circles[i]) << std::endl;
        std::cout << "Address of object = " << &circles[i] << std::endl;
        std::cout << "Address of radius variable = " << &circles[i].get_radius() << std::endl;
    }
    
    Rectangle rectangles[5] = {
        Rectangle(10, 20),
        Rectangle(20, 20),
        Rectangle(30, 90),
        Rectangle(40, 40),
        Rectangle(50, 70),
    };
    
    for (int i = 0; i < 5; i++) {
        std::cout << "Rectangle at index " << i << ":" << std::endl;
        std::cout << "Size of object = " << sizeof(rectangles[i]) << std::endl;
        std::cout << "Address of object = " << &rectangles[i] << std::endl;
        std::cout << "Address of width = " << &rectangles[i].get_width() << std::endl;
        std::cout << "Address of height = " << &rectangles[i].get_height() << std::endl;
    }
    
    Figure *figures[10];
    
    for (int i = 0; i < 5; i++)
    {
        figures[i] = &circles[i];
        figures[i+5] = &rectangles[i];
    }
    
    for (int i = 0; i < 10; i++)
    {
        figures[i]->show();
    }
}