#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <cstdlib>

#include "lab3.h"


#define SHOW_FUNC_MESSAGES


class Date
{
public:
    int day;
    int month;
    int year;
public:
    
    Date()
    {
        this->day = 1;
        this->month = 1;
        this->year = 1990;
    }
    
    Date(int day, int month, int year)
    {
        this->day = day;
        this->month = month;
        this->year = year;
    }
    
    std::string description()
    {
        return + "d:" + std::to_string(day) + " m:" + std::to_string(month) + " y:" + std::to_string(year);
    }
};

class Goods
{
private:
    std::string title;
    Date date;
    double cost;
    int count;
    int number;
public:
    
#pragma mark - Constructors
    
    Goods()
    {
#ifdef SHOW_FUNC_MESSAGES
        std::cout << "Goods constructor called" << std::endl;
#endif
        this->title = "Default title";
        this->date = Date(01, 01, 1990);
        this->cost = 99.99;
        this->count = 0;
        this->number = 1;
    }
    
    Goods(const Goods &goods)
    {
#ifdef SHOW_FUNC_MESSAGES
        std::cout << "Goods copy constructor called" << std::endl;
#endif
        this->title = goods.title;
        this->date = goods.date;
        this->cost = goods.cost;
        this->count = goods.count;
        this->number = goods.count;
    }
    
#pragma mark - Destructors
    
    ~Goods()
    {
#ifdef SHOW_FUNC_MESSAGES
        std::cout << "Goods destructor called" << std::endl;
#endif
    }
    
    void change(std::string newTitle, Date newDate, double newCost, int newCount, int newNumber)
    {
        this->title = newTitle;
        this->date = newDate;
        this->cost = newCost;
        this->count = newCount;
        this->number = newNumber;
    }
    
    void show()
    {
        printf("<Goods %p: title = %s; date = %s; cost = %f; count = %d; number = %d>\n",
               this,
               title.c_str(),
               date.description().c_str(),
               cost,
               count,
               number);
    }
    
    double price()
    {
        return count * cost;
    }
    
    Goods& operator=(Goods &goods)
    {
#ifdef SHOW_FUNC_MESSAGES
        std::cout << "Goods operator = called" << std::endl;
#endif
        title = goods.title;
        date = goods.date;
        cost = goods.cost;
        count = goods.count;
        number = goods.number;
        
        return *this;
    }
    
};

Goods tovar1(Goods s)
{
    return s;
}

Goods tovar2(Goods &s)
{
    return s;
}

Goods *tovar3(Goods s)
{
    return &s;
}

Date randomDate()
{
    return Date(rand() % 30 + 1, rand() % 11 + 1, rand() % 100 + 2000);
}

void first_case()
{
    Goods goods = Goods();
    Goods anotherGoods = goods;
    Goods thirdGoods;
    thirdGoods = goods;
}

void second_case()
{
    
}

void lab3()
{
#ifdef SHOW_FUNC_MESSAGES
    std::cout << "main function" << std::endl;
#endif

    {
        Goods goods[5];

        for (int i = 0; i < 5; i++) {
            goods[i].change("Edited Goods",
                            randomDate(),
                            rand() % 100 / (rand() % 10 + 1),
                            rand() % 200,
                            rand() % 256);
        }
        
        for (int i = 0; i < 5; i++) {
            goods[i].show();
            std::cout << "price = " << goods[i].price() << std::endl << std::endl;
        }
    }
    
    std::cout << " ==================== " << std::endl << std::endl;
    
    {
        Goods firstGoods;
        firstGoods.change("First good", Date(20, 11, 2013), 200.0, 30, 5);
        Goods secondGoods = firstGoods;
        Goods thirdGoods;
        thirdGoods = firstGoods;
        
        std::cout << std::endl << " -------------------- " << std::endl;
        
        firstGoods.show();
        secondGoods.show();
        thirdGoods.show();
        
        std::cout << std::endl << " -------------------- " << std::endl;
        
    }
    
    std::cout << std::endl << " ==================== " << std::endl << std::endl;
    
    {
        std::cout << " (zero) ++++++++++++++++++++ " << std::endl;
        Goods zeroGoods;
        std::cout << " (first) ++++++++++++++++++++ " << std::endl;
        Goods firstGoods = tovar1(zeroGoods);
        std::cout << " (second) ++++++++++++++++++++ " << std::endl;
        Goods secondGoods = tovar2(zeroGoods);
        std::cout << " (third) ++++++++++++++++++++ " << std::endl;
        Goods *thirdGoods = tovar3(zeroGoods);
        std::cout << " ++++++++++++++++++++ " << std::endl;
    }
    
    
}
