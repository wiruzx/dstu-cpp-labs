//
//  lab6.cpp
//  CPPLabs
//
//  Created by Виктор Шаманов on 1/23/14.
//  Copyright (c) 2014 Victor Shamanov. All rights reserved.
//

#include "lab6_1.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

#pragma mark - First

class Base1
{
    int i;
public:
    Base1()
    {
        i = 0;
        std::cout << "Constructor without arguments of Base1" << std::endl;
    }
    
    Base1(int value)
    {
        std::cout << "Constructor with arguments of Base1" << std::endl;
        i = value;
    }
    
    void put(int value)
    {
        i = value;
    }
    
    int get()
    {
        return i;
    }
};

class Base2
{
    char name[20];
public:
    Base2()
    {
        std::cout << "Constructor without arguments of Base2" << std::endl;
        strcpy(name, "Пусто");
    }
    
    Base2(char *value)
    {
        std::cout << "Constructor with arguments of Base2" << std::endl;
        strcpy(name, value);
    }
    
    void put(char *value)
    {
        strcpy(name, value);
    }
    
    char* get()
    {
        return name;
    }
};

class Derived: Base2, Base1
{
    char ch;
public:
    Derived()
    {
        std::cout << "Constructor without arguments of Derived" << std::endl;
        ch = 'V';
    }
    
    Derived(char first, char *second, int third): Base1(third), Base2(second)
    {
        std::cout << "Constructor with arguments of Derived" << std::endl;
        ch = first;
    }
    
    void put(char value)
    {
        ch = value;
    }
    
    char get()
    {
        return ch;
    }
    
    friend std::ostream& operator<<(std::ostream& os, Derived &derived)
    {
        os << "Derived:" << std::endl;
        os << "    ch = " << derived.Derived::get() << std::endl;
        os << "    name = " << derived.Base2::get() << std::endl;
        os << "    i = " << derived.Base1::get() << std::endl << std::endl;
        
        return os;
    }
    
};

#pragma mark - Second

class DomesticAnimal
{
protected:
    double weight;
    double price;
    std::string color;
public:
    
    DomesticAnimal()
    {
        
    }
    
    DomesticAnimal(double w, double p, std::string c)
    {
        weight = w;
        price = p;
        color = c;
    }
    
    void print()
    {
        std::cout << "DomesticAnimal:" << std::endl;
        std::cout << "    weight = " << weight <<std::endl;
        std::cout << "    price = " << price << std::endl;
        std::cout << "    color = " << color << std::endl;
    }
};

class Cow: public virtual DomesticAnimal
{
public:
    void print()
    {
        std::cout << "Cow:" << std::endl;
        DomesticAnimal::print();
    }
};

class Buffalo: public virtual DomesticAnimal
{
public:
    void print()
    {
        std::cout << "Buffalo:" << std::endl;
        DomesticAnimal::print();
    }
};

class Beefalo: public Cow, public Buffalo
{
public:
    
    Beefalo()
    {
        weight = 10.0;
        price = 200.0;
        color = "gray";
    }
    
    void print()
    {
        std::cout << "Beefalo:" << std::endl;
        DomesticAnimal::print();
    }
};

void lab6_1()
{
    //first
//    std::cout << "One:" << std::endl;
//    
//    Derived one;
//    std::cout << one << std::endl;
//    
//    std::cout << "Two:" << std::endl;
//    
//    Derived two('T', "Test", 10);
//    std::cout << two << std::endl;
    
    //second
    
    Cow cow;
    Buffalo buffalo;
    
    cow.print();
    buffalo.print();
    
    
}