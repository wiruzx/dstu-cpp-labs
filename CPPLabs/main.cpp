//
//  main.cpp
//  CPPLabs
//
//  Created by Виктор Шаманов on 1/18/14.
//  Copyright (c) 2014 Victor Shamanov. All rights reserved.
//

#include "lab3.h"
#include "lab4.h"
#include "lab5.h"
#include "lab6_1.h"
#include "lab6_2_1.h"

int main(int argc, const char * argv[])
{
//    lab3();
//    lab4();
//    lab5();
//    lab6_1();
    lab6_2_1();
    return 0;
}

