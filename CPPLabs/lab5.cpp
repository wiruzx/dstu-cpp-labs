//
//  lab5.cpp
//  CPPLabs
//
//  Created by Виктор Шаманов on 1/18/14.
//  Copyright (c) 2014 Victor Shamanov. All rights reserved.
//

#include "lab5.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

void ff()
{
    std::cout << "Global function" << std::endl;
}

class Base
{
private:
    double d;
protected:
    long l;
public:
    int i;
    
    Base()
    {
        d = 0.6666;
        l = 2000000000;
        i = 16777216;
    }
    
    Base(double d)
    {
        this->d  = d;
    }
    
    double getD()
    {
        return d;
    }
    
//    void ff()
//    {
//        std::cout << "Function of Base class" << std::endl;
//    }
    
};

class Derived: public Base
{
private:
    float f;
    
public:
    
    Derived()
    {
        f = 0.20;
    }
    
    Derived(double d, long l, int i, float f): Base(d)
    {
        this->l = l;
        this->i = i;
        this->f = f;
    }
    
    friend std::ostream& operator<< (std::ostream &os, Derived &derived)
    {
        os << "l=" << derived.l << ",";
        os << "i=" << derived.i << ",";
        os << "f=" << derived.f << ",";
        os << "d=" << derived.getD() << ";";
        return os;
    }
    
//    void ff()
//    {
//        std::cout << "Function of Derived class" << std::endl;
//    }
};

class Derived_1: public Derived
{
public:
    
    Derived_1(double d, long l, int i, float f): Derived(d, l, i, f)
    {
        
    }
    
    void foo();
    
//    void foo()
//    {
//        i++;
//        l += 1;
//        
//        ::ff();
//        Base::ff();
//        Derived::ff();
//    }
    
};

void Derived_1::foo()
{
    ff();
}

void lab5()
{
    Base b;
    std::cout << "size of Base = " << sizeof(Base) << std::endl << "size of Derived = " << sizeof(Derived) << std::endl;
    
    Derived d(200.6, 200, 100, 100.6);
    std::cout << std::endl << d << std::endl;
    
    Derived anotherD;
    std::cout << std::endl << anotherD << std::endl;
    
    Derived_1 d_1(20.5, 10, 20, 3.6);
    d_1.foo();
    
    
}
