//
//  lab4.cpp
//  CPPLabs
//
//  Created by Виктор Шаманов on 1/19/14.
//  Copyright (c) 2014 Victor Shamanov. All rights reserved.
//

#include "lab4.h"
#include <iostream>
#include <stdlib.h>
#include <string.h>

class Date
{
    int day;
    int month;
    int year;
    
    int days[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
    
public:
    Date(int d, int m, int y)
    {
        day = d;
        month = m;
        year = y;
    }
    
    Date(): Date(1, 1, 1980)
    {
        
    }
    
    Date(Date &date)
    {
        day = date.day;
        month = date.month;
        year = date.year;
    }
    
    long number_of_days()
    {
        long result = year * 365;
        for (int i = 1; i< month; i++) result += days[i];
        result += day;
        return result;
    }
    
    Date(const long number_of_days)
    {
        month = 0;
        year = 0;
        
        long remaining_days = number_of_days;
        
        while (remaining_days > 365) {
            remaining_days -= 365;
            year++;
        }
        
        while (remaining_days > days[month]) {
            remaining_days -= days[month];
            month++;
        }
        
        day = (int)remaining_days;
    }
    
    void show_date()
    {
        std::cout << *this << std::endl;
    }
    
    friend std::ostream& operator<< (std::ostream &os, Date &date)
    {
        os << "(Date: " << date.day << "." << date.month << "." << date.year << ")";
        return os;
    }
   
    Date& operator+(Date &date)
    {
        Date result = *this;
        
        result.day += date.day;
        result.year += date.year;
        
        result.month += date.month;
        
        if (result.month > 12) {
            result.month -= 12;
            result.year ++;
        }
        
        while (result.day > days[result.month]) {
            result.day -= days[result.month];
            result.month++;
        }
        
        
        return result;
    }
    
    Date& operator-(Date &date)
    {
        Date result = *this;
        
        result.day -= date.day;
        
        while (result.day <= 0) {
            result.month--;
            result.day += days[result.month];
        }
        
        result.month -= date.month;
        
        if (month <= 0) {
            result.month += 12;
            result.year--;
        }
        
        result.year -= date.year;
        
        return result;
    }
    
    Date& operator+(int number)
    {
        Date result(this->number_of_days() + number);
        return result;
    }
    
    Date& operator-(int number)
    {
        Date result(this->number_of_days() - number);
        return result;
    }
    
    Date& operator++(int number)
    {
        return *this + 1;
    }
    
    Date& operator--(int number)
    {
        return *this - 1;
    }
    
};

Date& operator+(int number_of_days, Date &date)
{
    return date + number_of_days;
}

//long& operator-(Date &first_date, Date &second_date)
//{
//    long result = first_date.number_of_days() - second_date.number_of_days();
//    return result;
//}



void lab4()
{
    Date first_date(10, 11, 2012), second_date(13, 8, 2004), third_date(2, 4, 6);
    Date one = first_date - third_date;
    Date two = first_date + second_date;
    Date three = first_date - 10;
    Date four = second_date + 20;
    Date five = 50 + second_date;
   
    one.show_date();
    two.show_date();
    three.show_date();
    four.show_date();
    five.show_date();
    
    std::cout << "first date" << std::endl;
    first_date.show_date();
    first_date++;
    first_date.show_date();
    
    std::cout << "second date" << std::endl;
    second_date.show_date();
    second_date--;
    second_date.show_date();
    
}